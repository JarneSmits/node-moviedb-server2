const express = require('express')
const router = express.Router()
const ApartmentController = require('../controllers/apartment.controller')

// Lijst van apartments
router.get('/apartments', ApartmentController.getApartments)

module.exports = router
